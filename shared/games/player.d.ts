export class Player {
    steamId: number;
    steamName: string;
    points: number;
    currentBet: object;
	
	constructor(steamId: number, steamName: string);
}
