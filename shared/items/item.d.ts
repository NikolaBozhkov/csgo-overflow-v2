export class Item {
	hashName: string;
	name: string;
	iconUrl: string;
	nameColor: string;
	qualityColor: string;
	price: number;
	type: string;

	constructor(hashName: string, type: string, meta: object);
}
