export const constants = {
	itemSellRatio: 0.9,
	blueprintUpgradeRatio: 0.4,
	blueprintSellRatio: 0.4
};

export const itemType = {
	skinBlueprint: "skinBlueprint",
	skinTicket: "skinTicket",
	material: "material"
};

export let itemsUtil = {
	getSellPrice(item, price) {
		if (price === undefined) {
			price = item.price;
		}

		if (item.type == itemType.skinBlueprint) {
			return price * constants.blueprintSellRatio;
		} else {
			return price * constants.itemSellRatio;
		}
	}
};
