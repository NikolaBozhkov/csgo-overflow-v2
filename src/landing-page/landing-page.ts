import { AuthService } from './auth-service';
import { Aurelia, autoinject } from 'aurelia-framework';
import { PLATFORM } from 'aurelia-pal';

@autoinject
export class LandingPage {
	constructor(private aurelia: Aurelia, private authService: AuthService) {
	}

	logIn() {
		// TODO: steam login promise
		this.authService.steamLogin();
		//this.aurelia.setRoot('app');
	}

	// For testing purposes
	skipLogIn() {
		this.aurelia.setRoot('app');
	}
}
