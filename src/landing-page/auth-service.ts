import { AureliaCookie } from 'aurelia-cookie';

export class AuthService {

	isUserLoggedIn(): boolean {
		return !!AureliaCookie.get('token');
	}

	steamLogin() {
		window.location.href = 'api/auth/steam';
	}
}
