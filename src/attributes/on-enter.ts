import { customAttribute, autoinject } from "aurelia-framework";

@autoinject
@customAttribute('on-enter')
export class OnEnter {
	element: Element;
	onEnter;
	action;

	constructor(element: Element) {
		this.element = element;

		this.onEnter = (ev: KeyboardEvent) => {
			//Enter keyCode is 13
			if (ev.keyCode !== 13) return;
			this.action();
		};
	}

	valueChanged(func) {
		this.action = func;
	}
	
	attached() {
		this.element.addEventListener("keyup", this.onEnter);
	}

	detached() {
		this.element.removeEventListener("keyup", this.onEnter);
	}
}
