// Might not be needed

export default class User {
	_id: string;
	steamName: string;
	steamId: string;
	tradeUrl: string;
	ticketCurrency: number;
	skinCurrency: number;
	tickets: number;
	inventory: any[];
	avatarUrl: string;
	referrer: string;
	referrals: number;
	referralCurrency: number;
	withdrawnAmount: number;
	clearance: string;
}
