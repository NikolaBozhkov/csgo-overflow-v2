import { Aurelia } from 'aurelia-framework';
import { Router, RouterConfiguration } from 'aurelia-router';
import { PLATFORM } from 'aurelia-pal';

export class App {
	router: Router;
	sideMenu: Element

	configureRouter(config: RouterConfiguration, router: Router) {
		config.title = 'CSGO Overflow';
		config.options.pushState = true;

		config.map([
			{ route: 'account', name: 'account', moduleId: PLATFORM.moduleName('account/account'), nav: true, title: 'Account' },
			{ route: 'active-games', name: 'active-games', moduleId: PLATFORM.moduleName('active-games/active-games'), nav: true, title: 'Active Games' },
			{ route: 'active-game/:gameId', name: 'active-game', moduleId: PLATFORM.moduleName('active-game/active-game'), title: 'Game' },
			{ route: 'inventory', name: 'inventory', moduleId: PLATFORM.moduleName('inventory/inventory'), nav: true, title: 'Inventory' },
			{ route: 'shop', name: 'shop', moduleId: PLATFORM.moduleName('shop/shop'), nav: true, title: 'Shop' },
			{ route: 'earn', name: 'surveys', moduleId: PLATFORM.moduleName('surveys/surveys'), nav: true, title: 'Earn' }]);

		config.mapUnknownRoutes(PLATFORM.moduleName('active-games/active-games'));
		this.router = router;
	}

	toggle() {
		$(this.sideMenu).toggleClass('chat-hidden');
	}
}
