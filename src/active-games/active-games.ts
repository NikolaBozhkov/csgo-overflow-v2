import { GamesService } from './games-service';
import { autoinject } from 'aurelia-framework';
import { Router } from 'aurelia-router';

@autoinject
export class ActiveGames {
	gameType : number
    
    constructor(private gamesService: GamesService, private router: Router) {}

    async addPlayer() {
        let gameId = await this.gamesService.addPlayer(this.gameType);
        this.router.navigate('active-game/'+ gameId);
    }
}
