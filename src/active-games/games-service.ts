import { autoinject } from 'aurelia-framework';
import { HttpClient } from 'aurelia-fetch-client';

@autoinject
export class GamesService {
	constructor(private httpClient: HttpClient) {
	}

	addPlayer(gameType: number): Promise<any> {
        return this.httpClient
        .fetch('games', {
			method: "POST",
			body: JSON.stringify({ gameType })
        })
        .then(res => res.json());
	}
}
