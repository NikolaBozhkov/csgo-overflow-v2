import { Message } from './../../shared/messages/message.d';
import { MessagesService } from './messages-service';
import { autoinject } from 'aurelia-framework';
import { socket } from '../socket/socket-service';

@autoinject
export class Chat {
	messages: Message[] = [];
	textMessage: string;

	constructor(private messagesService: MessagesService) {
		this.getMessages();
		socket.on('newMessage', msg => this.messages.push(msg as Message));
	}

	async getMessages() {
		let messages = await this.messagesService.getAllMessages();

		if (!messages) {
			// DO sth to say it failed
			console.log('failed to load messages');
			return;
		}

		this.messages = messages;
    }
    
    sendMessage() {
		this.messagesService.sendMessage(this.textMessage);
		this.textMessage = "";
	}

}
