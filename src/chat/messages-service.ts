import { Message } from './../../shared/messages/message.d';
import { autoinject } from 'aurelia-framework';
import { HttpClient } from 'aurelia-fetch-client';

@autoinject
export class MessagesService {
	constructor(private httpClient: HttpClient) {
	}

	getAllMessages(): Promise<Message[]> {
		return this.httpClient
			.fetch('messages')
			.then(res => res.json())
			.then(messages => messages as Message[]);
	}

	sendMessage(text: string) {
		this.httpClient.fetch('messages', {
			method: "POST",
			body: JSON.stringify({ text })
		});
	}
}
