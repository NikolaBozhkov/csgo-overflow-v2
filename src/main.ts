﻿import { AureliaCookie } from 'aurelia-cookie';
/// <reference types="aurelia-loader-webpack/src/webpack-hot-interface"/>
// we want font-awesome to load as soon as possible to show the fa-spinner
import './styles/main.scss';
import { Aurelia } from 'aurelia-framework';
import { PLATFORM } from 'aurelia-pal';
import * as Bluebird from 'bluebird';
import { HttpClient } from 'aurelia-fetch-client';

// remove out if you don't want a Promise polyfill (remove also from webpack.config.js)
Bluebird.config({ warnings: { wForgottenReturn: false } });

export async function configure(aurelia: Aurelia) {
	aurelia.use
		.standardConfiguration()
		.developmentLogging()
		.plugin(PLATFORM.moduleName('aurelia-cookie'));

	let container = aurelia.container;

	let http = new HttpClient();
	http.configure(config => {
		config
			.useStandardConfiguration()
			.withBaseUrl('http://localhost:8080/api/')
			.withDefaults({
				headers: {
					'Content-Type': 'application/json',
				},
				credentials: 'same-origin'
			});
	});

	container.registerInstance(HttpClient, http);

	// Uncomment the line below to enable animation.
	// aurelia.use.plugin(PLATFORM.moduleName('aurelia-animator-css'));
	// if the css animator is enabled, add swap-order="after" to all router-view elements

	// Anyone wanting to use HTMLImports to load views, will need to install the following plugin.
	// aurelia.use.plugin(PLATFORM.moduleName('aurelia-html-import-template-loader'));

	await aurelia.start();

	if (isUserLoggedIn()) {
		await aurelia.setRoot(PLATFORM.moduleName('app'));
	} else {
		await aurelia.setRoot(PLATFORM.moduleName('landing-page/landing-page'));
	}
}

function isUserLoggedIn(): boolean {
	return !!AureliaCookie.get('token');
}
