import { HttpClient } from 'aurelia-fetch-client';
import { autoinject } from 'aurelia-framework';

@autoinject
export class AccountService {
	tickets: number = 0;
	skinCurrency: number = 0;
	ticketCurrency: number = 0;

	constructor(private httpClient: HttpClient) {
		this.getUserCurrency()
	}

	getUserCurrency() {
		this.httpClient
			.fetch('user/currency')
			.then(res => res.json())
			.then(currInfo => {
				this.tickets = currInfo.tickets
				this.skinCurrency = currInfo.skinCurrency
				this.ticketCurrency = currInfo.ticketCurrency
			});
	}
}
