import { AccountService } from './../account/account-service';
import { HttpClient } from 'aurelia-fetch-client';
import { autoinject } from 'aurelia-framework';
import { Item } from '../../shared/items/item.d';

@autoinject
export class InventoryService {
	constructor(private httpClient: HttpClient, private accountService: AccountService) {

	}

	getInventory(): Promise<[Item]> {
		return this.httpClient
			.fetch('inventory')
			.then(res => res.json())
			.then(inventory => {
				console.log(inventory);
				return inventory as [Item]});
	}

	addNewItem(item: any): Promise<any> {
		return this.httpClient
			.fetch('inventory/addItem', {
				method: 'POST',
				body: JSON.stringify(item)
			})
			.then(res => res.json());
	}

	getAllItems(): Promise<[string]> {
		return this.httpClient
			.fetch('inventory/allItems')
			.then(res => res.json())
			.then(inventory => Object.keys(inventory) as [string]);
	}

	sellItem(item: Item): Promise<any> {
		return this.httpClient
			.fetch('inventory/sell', {
				method: 'PUT',
				body: JSON.stringify({
					name: item.name,
					type: item.type
				})
			})
			.then(res => res.json())
			.then(data => {
				this.accountService.skinCurrency += data.sellPrice;
				console.log(data);
				return data;
			});
	}

	forgeItems(items: Item[]): Promise<Item> {
		return this.httpClient
			.fetch('inventory/forge', {
				method: 'PUT',
				body: JSON.stringify({ items })
			})
			.then(res => res.json());
	}
}
