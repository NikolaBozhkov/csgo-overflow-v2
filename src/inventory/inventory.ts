import { autoinject } from 'aurelia-framework';
import { InventoryService } from './inventory-service';
import { Item } from '../../shared/items/item.d';
import { constants, itemsUtil, itemType } from '../../shared/util.js';

@autoinject
export class Inventory {
	itemsUtil = itemsUtil;
	itemType = itemType;
	allItems: string[];
	items: Item[];
	newItem: any;
	itemsForge: Item[] = [];

	constructor(private inventoryService: InventoryService) {
		this.newItem = {
			name: '',
			type: itemType.skinTicket
		};

		this.loadAllItems();

		this.inventoryService.getAllItems()
			.then(items => this.allItems = items);
	}

	addToForge(item: Item) {
		let index = this.items.indexOf(item);
		if (index != -1) this.items.splice(index, 1);
		this.itemsForge.push(item);
	}

	removeFromForge(item: Item) {
		let index = this.itemsForge.indexOf(item);
		if (index != -1) this.itemsForge.splice(index, 1);
		this.items.push(item);
	}

	async forgeItems() {
		let item = await this.inventoryService.forgeItems(this.itemsForge);
		if (item === undefined) return;

		this.itemsForge = [];
		this.items.push(item);
	}

	async addNewItem() {
		await this.inventoryService.addNewItem(this.newItem);
		this.loadAllItems();
	}

	loadAllItems() {
		this.inventoryService.getInventory()
			.then(items => this.items = items);
	}

	async sellItem(item: Item) {
		let data = await this.inventoryService.sellItem(item);
		this.loadAllItems();
	}
}
