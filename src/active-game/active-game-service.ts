import { autoinject } from 'aurelia-framework';
import { HttpClient } from 'aurelia-fetch-client';
import { Game } from './../../shared/games/game.d';

@autoinject
export class ActiveGameService {
	constructor(private httpClient: HttpClient) {
	}

	getQueuedGame(gameId: string): Promise<any> {
       return this.httpClient
			.fetch('games/' + gameId)
            .then(res => res.json())
            .then(game => game as Game);
	}
}