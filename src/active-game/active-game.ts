import { ActiveGameService } from './active-game-service';
import { autoinject } from 'aurelia-framework';
import { Game } from './../../shared/games/game.d';

@autoinject
export class ActiveGame {
    
    game: Game;  
    gameId: string;

    constructor(private activeGameService: ActiveGameService) {}

    activate(params){
        this.gameId = params.gameId;
        this.getQueuedGame(this.gameId);
    }

    async getQueuedGame(gameId){
        this.game = await this.activeGameService.getQueuedGame(gameId);
    }
}
