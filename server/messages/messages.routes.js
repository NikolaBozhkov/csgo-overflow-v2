import express from 'express';
import messagesController from './messages.controller';
import authController from '../auth/auth.controller';
const router = express.Router();

// /api/messages
router.get('', messagesController.getAll);
router.post('', authController.isAuthenticated, messagesController.createNew);

export default router;
