import User from '../user/user.model';
import { Message } from '../../shared/messages/message';
import { io } from '../config/socket';

let messages = [];

export default {
	getAll(req, res) {
		return res.json(messages);
	},

	async createNew(req, res) {
		// Check if body contains text prop
		if (!req.body.text) {
			return res.status(400).json({ error: 'request body must contain a text field' });
		}

		try {
			let user = await User.findOne({ steamId: req.user.steamId });
			let msg = new Message(user.steamName, user.avatarUrl, req.body.text);
			messages.push(msg);

			io.emit('newMessage', msg);
		} catch (err) {
			return res.status(500).json({ error: err });
		}
	}
};
