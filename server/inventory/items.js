import fs from 'fs';
import { itemType } from '../../shared/util';

export let items = {};

export function loadItems() {
	let itemsString = fs.readFileSync(__dirname + '/items.txt', 'utf8');
	let itemsPricesString = fs.readFileSync(__dirname + '/items-prices.txt', 'utf8');

	let itemsPrices = JSON.parse(itemsPricesString).response.items;

	for (let item of JSON.parse(itemsString).items) {
		let itemPrice = itemsPrices[item['market_hash_name']];
		items[item['market_hash_name']] = {
			name: item['market_name'],
			iconUrl: item['icon_url'],
			nameColor: item['name_color'],
			qualityColor: item['quality_color'],
			price: itemPrice && itemPrice.value || -1
		}
	}
}
