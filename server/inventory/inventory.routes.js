import express from 'express';
import authController from '../auth/auth.controller';
import inventoryController from './inventory.controller';

const router = express.Router();

// api/inventory
router.get('', authController.isAuthenticated, inventoryController.getInventory);
router.put('/sell', authController.isAuthenticated, inventoryController.sellItem);
router.put('/upgradeSkinBlueprint', authController.isAuthenticated, inventoryController.upgradeSkinBlueprint);
router.put('/forge', authController.isAuthenticated, inventoryController.forgeItems)

// FOR TESTING
router.post('/addItem', authController.isAuthenticated, inventoryController.addItem);
router.get('/allItems', inventoryController.getAllItems);

export default router;
