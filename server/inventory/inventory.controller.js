import User from '../user/user.model';
import { items } from './items';
import { constants, itemType, itemsUtil } from '../../shared/util';
import { Item } from '../../shared/items/item';
import forge from './forge';

export default {

	async getInventory(req, res) {
		try {
			let user = await User.findOne({ steamId: req.user.steamId });
			let returnInventory = [];

			// Populate with item information
			for (let item of user.inventory) {
				returnInventory.push(new Item(item.name, item.type, items[item.name]));
			}

			return res.json(returnInventory);
		} catch (err) {
			return res.status(500).json(err);
		}
	},

	async sellItem(req, res) {
		// Validate body
		if (!req.body.name || !req.body.type) {
			return res.status(400).json({ error: 'Body needs to contain "name" and "type" fields.' });
		}

		try {
			let user = await User.findOne({ steamId: req.user.steamId });
			
			// Validate if item is in the inventory
			let index = user.inventory.findIndex(
				item => item.type == req.body.type 
				&& item.name == req.body.name);

			if (index == -1) {
				return res.status(400).json({ error: 'The item is not in the user inventory.' });
			}

			// Get item info
			let item = user.inventory[index];
			let itemPrice = items[item.name].price;
			let sellPrice = itemsUtil.getSellPrice(item, itemPrice);
			
			// Update user info and save
			user.skinCurrency += sellPrice;
			user.inventory.splice(index, 1);
			
			await user.save();
			
			return res.json({ item, sellPrice });
		} catch (err) {
			console.log(err);
			return res.status(500).json(err);
		}
	},

	async upgradeSkinBlueprint(req, res) {
		// Validate body
		if (!req.body.name) {
			return res.status(400).json({ error: 'Body needs to contain "name" fields.' });
		}

		try {
			let user = await User.findOne({ steamId: req.user.steamId });

			// Validate if item is in the inventory
			let index = user.inventory.findIndex(
				item => item.name == req.body.name
				&& item.type == itemType.skinBlueprint);

			if (index == -1) {
				return res.status(400).json({ error: 'The blueprint is not in the user inventory.' });
			}

			// Get item info
			let item = user.inventory[index];
			let itemPrice = items[item.name].price;

			// Calculate upgrade price
			let upgradePrice = itemPrice * constants.blueprintUpgradeRatio;

			// Validate if user has enough money
			if (user.skinCurrency < upgradePrice) {
				return res.status(400).json({ error: 'Not enough skin currency to upgrade blueprint.' });
			}

			// Update user info and save
			user.skinCurrency -= upgradePrice;
			item.type = itemType.skinTicket;
			await user.save();

			return res.json({ item, upgradePrice });
		} catch (err) {
			return res.status(500).json(err);
		}
	},

	async forgeItems(req, res) {
		// Validate body contains items
		if (!req.body.items) {
			return res.status(400).json({ error: 'Body must contain "items" field.' });
		}

		try {
			let user = await User.findOne({ steamId: req.user.steamId });

			// Validate items are in inventory
			for (let item of req.body.items) {
				let index = user.inventory.findIndex(i => i.name == item.name);
				if (index == -1) {
					return res.status(400).json({ error: 'Some of the items are not in the inventory.' });
				}
			}

			// TODO: Validate if selected items are forgable together

			// Get resulting item of the forge
			let resultItem = forge.forgeItems(req.body.items);

			// Update user inventory
			user.inventory = user.inventory.filter(i => {
				// filter by items that are not contained in the req body
				let index = req.body.items.findIndex(toRemove => toRemove.name == i.name);
				return index == -1;
			});

			user.inventory.push(resultItem);
			await user.save();

			return res.json(resultItem);
		} catch (err) {
			console.log(err);
			return res.status(500).json(err);
		}
	},

	// FOR TESTING PURPOSES
	async addItem(req, res) {
		try {
			let user = await User.findOne({ steamId: req.user.steamId });
			console.log(req.body);
			user.inventory.push(req.body);

			await user.save();
			console.log('save');
			let item = new Item(req.body.name, req.body.type, items[req.body.name]);
			return res.json({ item });
		} catch (err) {
			return res.status(500).json(err);
		}
	},

	async getAllItems(req, res) {
		return res.json(items);
	}
}
