import express from 'express';
import http from 'http';
import mongoose from 'mongoose';
import config from './config/config';
import configPassport from './config/passport';
import configExpress from './config/express';
import { configSocket } from './config/socket';
import { items, loadItems } from './inventory/items'; 

const app = express();
const server = http.createServer(app);

mongoose.Promise = global.Promise;
mongoose.connect(config.database, {
	useMongoClient: true
});

// Load items
try {
	loadItems();
} catch (err) {
	console.log(err);
}

//console.log(items);

//require('./config/socket').init(server);
//require('./config/socketEvents')();
configExpress(app, express);
configPassport();

configSocket(server);

server.listen(config.port, () => {
	console.log('listening on port ' + config.port);
});

// app.use(function(req, res, next) {
//     res.header("Access-Control-Allow-Origin", "*")
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
//     next()
// })
