import mongoose from 'mongoose';
import { itemType } from '../../shared/util';
const Schema = mongoose.Schema;

// set up mongoose model
const userSchema = new Schema({
    steamName: { type: String, required: true },
    steamId: { type: String, required: true },
    tradeUrl: { type: String, default: "" },
	ticketCurrency: { type: Number, default: 0 },
	skinCurrency: { type: Number, default: 0 },
	tickets: { type: Number, default: 0 },
	inventory: [{ 
		name: { type: String, required: true },
		type: { type: String, enum: [itemType.skinTicket, itemType.skinBlueprint, itemType.material], required: true }
	}],
	avatarUrl: { type: String, default: "" },
	referrer: { type: String, default: "" },
	referrals: { type: Number, default: 0 },
	referralCurrency: { type: Number, default: 0 },
	withdrawnAmount: { type: Number, default: 0 },
    clearance: { type: String, required: true, enum: ['user', 'admin', 'dev', 'owner'], default: 'user' },
    surveyLogs: [],
	flag: { type: Number, default: 0 }
}, { collection: 'users' });

export default mongoose.model('User', userSchema);
