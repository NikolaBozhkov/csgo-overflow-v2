import jwt from 'jsonwebtoken';
import config from '../config/config';
import User from './user.model';

export default {

	async getUser(req, res) {
		try {
			let user = await User.findOne({ steamId: req.user.steamId });
			return res.json(user);
		} catch (err) {
			return res.status(500).json(err);
		}
	},

	updateReferralInformation() {

	},

	async updateTradeUrl(req, res) {
		// Validate if trade url is given
		if (!req.body.tradeUrl) {
			return res.status(400).json({ error: 'tradeUrl not present is body.' })
		}

		try {
			await User.findOneAndUpdate({
				// Find by
				steamId: req.user.steamId 
			}, { 
				// Update
				tradeUrl: req.body.tradeUrl
			});

			return res.status(204).end();
		} catch (err) {
			console.log(err);
			return res.status(500).json(err);
		}
	},

	async getUserCurrency(req, res) {
		try {
			let user = await User.findOne({ steamId: req.user.steamId });

			let currencyInfo = {
				tickets: user.tickets,
				skinCurrency: user.skinCurrency,
				ticketCurrency: user.ticketCurrency
			};

			return res.json(currencyInfo);
		} catch (err) {
			return res.status(500).json(err);
		}
	}
}
