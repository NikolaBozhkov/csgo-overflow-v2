import express from 'express';
import userController from './user.controller';
import authController from '../auth/auth.controller';
const router = express.Router();

// /api/user
router.get('', authController.isAuthenticated, userController.getUser);
router.put('/updateTradeUrl', authController.isAuthenticated, userController.updateTradeUrl);
router.get('/currency', authController.isAuthenticated, userController.getUserCurrency);

export default router;
