export default {
	local: true,
	secret: 'so7many1creative2unicorns',
	database: 'mongodb://root:csgo132overflow@ds161042.mlab.com:61042/csgooverflow-devdb',
	port: 3000,
	maxMsgCount: 75,
	excludeProperties: '-_id -__v'
}
