import express from 'express';
import http from 'http';
import socketIo from 'socket.io';
import config from './config';

export let io;
export function configSocket(server) {
	io = socketIo(server);
	io.set("origins", "*:*");
}
