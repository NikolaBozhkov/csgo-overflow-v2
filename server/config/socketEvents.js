import { io } from './socket';
import messagesController from '../messages/messages.controller';

let onlineUsers = 0;

module.exports = function() {
    io.on('connection', function (socket) {
        onlineUsers += 1;
        // io.sockets.emit('userConnected');
        // socket.emit('onlineUsers', onlineUsers);
        // userController.onUserConnected(socket);

        //socket.on('adLoaded', dropperController.onAdLoaded);

        socket.on('disconnect', () => {
            onlineUsers -= 1;
            // io.sockets.emit('userDisconnected');
            // userController.onUserDisconnected(socket);
        });
    });
}
