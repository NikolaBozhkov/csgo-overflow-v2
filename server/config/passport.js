import passport from 'passport';
import config from './config';
import User from '../user/user.model';
import { Strategy as SteamStrategy } from 'passport-steam';

export default function() {
    // Steam auth
    passport.use(new SteamStrategy({
        returnURL:  'http://localhost:8080/api/auth/steam/return',
        realm:  'http://localhost:8080/',
        apiKey: 'CE9D7E2DDD6AB89F45AB84C211B52C29',
        passReqToCallback: true
    },
    async function(req, identifier, profile, done) {
		var steamUser = profile._json;

		// get the user from the database
		try {
			let user = await User.findOne({ steamId: steamUser.steamid });

			if (user) {				
				// Update steam profile info
				user.steamName = steamUser.personaname;
				user.avatarUrl = steamUser.avatar;
				await user.save();
				return done(null, user);

			} else {
				// Create new user
				let newUser = await User.create({
					steamName: steamUser.personaname,
					steamId: steamUser.steamid,
					avatarUrl: steamUser.avatar,
					referrer: req.cookies.refId
				});

				return done(null, newUser);
			}
		} catch (err) {
			console.log(err);
			return done(err, null);
		}
    }));

    passport.serializeUser(async (user, done) => {
        if (user) {
            return done(null, user.steamId);
        }
    });

    passport.deserializeUser(async (id, done) => {
		try {
			let user = await User.findOne({ steamId: id });
			return done(null, user);
		} catch (err) {
			return done(err, null);
		}
    });
};
