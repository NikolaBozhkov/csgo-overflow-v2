import express from 'express';
import passport from 'passport';
import authController from './auth.controller';

const router = express.Router();

router.get('/steam', passport.authenticate('steam'), (req, res) => {
	// The request will be redirected to Steam for authentication, so
	// this function will not be called.
});

router.get('/steam/return', passport.authenticate('steam', { failureRedirect: '/' }), authController.steamSuccessfullyAuthenticated);

export default router;
