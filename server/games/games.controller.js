import User from '../user/user.model';
import GamesManager from './games-manager';

export default {
	async addPlayer(req, res) {

		if (!req.body.gameType) {
			return res.status(400).json({ error: 'request body must contain game type' });
		}

		try {
            let user = await User.findOne({ steamId: req.user.steamId });

            let steamId = user.steamId;
            let steamName = user.steamName;
            let avatarUrl = user.avatarUrl;

            let gameId = GamesManager.addPlayer(steamId, steamName, avatarUrl, req.body.gameType);
            return res.json(gameId);

		} catch (err) {
			return res.status(500).json({ error: err });
		}
    },
    
    getQueuedGame(req, res) {
        let game = GamesManager.getQueuedGame(req.params.gameId);

        if (game === undefined){
            return res.status(404).json({ error: 'game not found' });
        }

        let playerIsInGame = game.players.some(player => {
            return player.steamId == req.user.steamId;
        });

        if (!playerIsInGame) {
            return res.status(404).json({ error: 'the player is not in this game' });
        }

        return res.json(game);
    }
};
