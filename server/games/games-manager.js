import shortid from 'shortid';
import { Game } from './../../shared/games/game';
import { Player } from './../../shared/games/player';

let queuedGames = [];

export default {
	addPlayer(steamId, steamName, avatarUrl, gameType) {

        let index = queuedGames.findIndex(x => {
            return x.type == gameType &&
                   x.players.length < 10
        });

        let player = new Player(steamId, steamName, avatarUrl);

        if (index !== -1) {
            queuedGames[index].players.push(player);
            return queuedGames[index].id;
        }
        else {
            let game = new Game(shortid.generate(), gameType);
            game.players.push(player);
            queuedGames.push(game);
            return game.id;
        }
    },

    getQueuedGame(gameId) {
        return queuedGames.find(game => game.id == gameId);
    }
}
