import express from 'express';
import gamesController from './games.controller';
import authController from '../auth/auth.controller';
const router = express.Router();

// /api/games
router.post('', authController.isAuthenticated, gamesController.addPlayer);
router.get('/:gameId', authController.isAuthenticated, gamesController.getQueuedGame)

export default router;
